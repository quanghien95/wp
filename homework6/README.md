#Create DB
```
CREATE DATABASE `shopping_cart`;
```

#create table product
```
CREATE TABLE `shopping_cart`.`product` ( `id` INT NOT NULL AUTO_INCREMENT , `name` VARCHAR(128) NOT NULL , `description` TEXT NOT NULL , `price` FLOAT NOT NULL , `quantity` INT NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;
```

#create table order
```
CREATE TABLE `shopping_cart`.`order` ( `id` INT NOT NULL AUTO_INCREMENT , `item_data` TEXT NOT NULL , `amount` FLOAT NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;
```

#insert data
```
INSERT INTO `product` (`id`, `name`, `description`, `price`, `quantity`) VALUES (NULL, 'AK47', 'rifle for T side only', '2700', 100), (NULL, 'M4A4', 'rifle for CT', '3100', 100), (NULL, 'DE', 'hand cannon', '700', 100), (NULL, 'AWP', '1 shot 1 kill', '4750', 100);
```

#all
```
CREATE DATABASE `shopping_cart`;
CREATE TABLE `shopping_cart`.`product` ( `id` INT NOT NULL AUTO_INCREMENT , `name` VARCHAR(128) NOT NULL , `description` TEXT NOT NULL , `price` FLOAT NOT NULL , `quantity` INT NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;
CREATE TABLE `shopping_cart`.`order` ( `id` INT NOT NULL AUTO_INCREMENT , `item_data` TEXT NOT NULL , `amount` FLOAT NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;
INSERT INTO `product` (`id`, `name`, `description`, `price`, `quantity`) VALUES (NULL, 'AK47', 'rifle for T side only', '2700', 100), (NULL, 'M4A4', 'rifle for CT', '3100', 100), (NULL, 'DE', 'hand cannon', '700', 100), (NULL, 'AWP', '1 shot 1 kill', '4750', 100);
```
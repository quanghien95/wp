<?php

session_start();

$products = [];

$servername = "localhost";
$username = "root";
$password = "root";
$dbname = "shopping_cart";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT * FROM product";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        array_push($products, $row);
    }
} else {
    echo "0 results";
}
$conn->close();

$_SESSION['products'] = $products;
if (isset($_POST['username'])&&($_POST['username'])) $_SESSION['username'] = $_POST['username'];
if (isset($_POST['password'])&&($_POST['password'])) $_SESSION['password'] = $_POST['password'];
?>
<html>
<h1><strong>Welcome to BKSHOP</strong></h1><br>
<?php 
if ((!isset($_SESSION['username'])) || (!isset($_SESSION['password']))){
//if (!$_SESSION['username'] || !$_SESSION['password']) { 

?>
    <form action="login.php" method="post">
        <input type="submit" name="login" value="Login"/>
    </form>
<?php } else { ?>
    Welcome <?php echo $_SESSION['username']; ?><br>
    <form action="login.php" method="post">
        <input type="submit" name="logout" value="Logout"/>
    </form>
<?php } ?>

<table border="1">
    <thead>
    <th>Name</th>
    <th>Description</th>
    <th>Price</th>
    <th></th>
    </thead>
    <tbody>
    <?php foreach ($products as $product) { ?>
        <tr>
            <td><?php echo $product['name']; ?></td>
            <td><?php echo $product['description']; ?></td>
            <td><?php echo $product['price']; ?></td>
            <td><?php echo $product['quantity']; ?></td>
            <td><form action="added.php" method="post">
                    <button type="submit" name="addtocart" value="<?php echo $product['id'];?>">Add to cart</button>
                </form></td>
        </tr>
    <?php } ?>
    </tbody>
</table>

<br><a href="cart.php">Show cart</a>
</html>

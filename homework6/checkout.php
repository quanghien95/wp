<?php

session_start();
$cartProducts = $_SESSION['added_products'];
$orderData=[];
$baseTotal = 0;
$servername = "localhost";
$username = "root";
$password = "root";
$dbname = "shopping_cart";
// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
?>
<h3>Thank you for your order</h3>
<h4>Order number #1234</h4>
<ul>
    <?php foreach ($cartProducts as $cartProduct) {
        $newQly = $cartProduct['quantity'] - $cartProduct['qty'];
        $sql = "UPDATE product SET quantity=$newQly WHERE id=$cartProduct[id]";
        $conn->query($sql);
        array_push($orderData, $cartProduct);
        $baseTotal += $cartProduct['price']*$cartProduct['qty'];
        ?>
        <li><?php echo $cartProduct['name'] . '; ' . $cartProduct['qty'] . ' unit(s);' ?></li>
    <?php } ?>
    <?php
    $orderData = serialize($orderData);
    $sql = "INSERT INTO `order` (item_data, amount)
VALUES ('$orderData', '$baseTotal')";
    if ($conn->query($sql) === TRUE) {
        echo "Create order success";
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }
    $conn->close();
    ?>
</ul>
<?php unset($_SESSION['added_products']); ?>
<a href="index.php">Back to main</a>

<?php

session_start();
$cartProducts = $_SESSION['added_products'];
?>

<h3>Shopping Cart</h3>
<?php if (!count($cartProducts)) { ?>
    <div>Cart is empty</div>
<?php } else { ?>
    <table border="1">
        <thead>
        <th>Name</th>
        <th>Description</th>
        <th>Price</th>
        <th>Qty</th>
        <th></th>
        </thead>
        <tbody>
        <?php foreach ($cartProducts as $cartProduct) { ?>
            <tr>
                <td><?php echo $cartProduct['name']?></td>
                <td><?php echo $cartProduct['description']?></td>
                <td><?php echo $cartProduct['price']?></td>
                <td><?php echo $cartProduct['qty']?></td>
                <td>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>

    <a href="checkout.php">Go to checkout</a>
<?php } ?>
<a href="index.php">Back to main</a>

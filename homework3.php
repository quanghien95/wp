<form action="homework3.php" method="post">
    Radian to degree: <input type="number" size="10" maxlenght="10" name="radian" step="0.000001"><br>
    Degree to radian: <input type="number" size="10" maxlenght="10" name="degree" step="0.000001"><br>
    Enter person 1 name: <input type="text" size="10" maxlenght="25" name="name1"><br>
    Enter person 1 birthday: <input type="date" size="10" maxlenght="10" name="birthday1"><br>
    Enter person 2 name: <input type="text" size="10" maxlenght="25" name="name2"><br>
    Enter person 2 birthday: <input type="date" size="10" maxlenght="10" name="birthday2"><br>
    <input type="submit" value="Submit">
</form>
<?php
if (!empty($_POST)) {
    $radian = $_POST['radian'];
    $degree = $_POST['degree'];
    $name1 = $_POST['name1'];
    $name2 = $_POST['name2'];
    $birthday1 = $_POST['birthday1'];
    $birthday2 = $_POST['birthday2'];
    $flag = 0;
    $time = new DateTime(date("Y-m-d", time()));
    if (!!$radian) {
        echo "$radian radians = " . rad2deg($radian) . " degrees<br>";
    }
    if (!!$degree) {
        echo "$degree degrees = " . deg2rad($degree) . " radians<br>";
    }
    if (!empty($name1) && !empty($birthday1)) {
        $DOB1 = date("l, F j, Y", strtotime($birthday1));
        if ($DOB1 == -1) {
            echo "Invalid date!<br>";
            $flag = 1;
        } else {
            $date1 = new DateTime($birthday1);
            $age1 = $date1->diff($time)->y;
            echo "$name1: $DOB1, $age1 years old<br>";
        }
    }
    if (!empty($name2) && !empty($birthday2)) {
        $DOB2 = date("l, F j, Y", strtotime($birthday2));
        if ($DOB2 == -1) {
            echo "Invalid date!<br>";
            $flag = 1;
        } else {
            $date2 = new DateTime($birthday2);
            $age2 = $date2->diff($time)->y;
            echo "$name2: $DOB2, $age2 years old<br>";
        }
    }
    if ($flag == 0) {
        $interval = $date1->diff($date2);
        echo "Difference in days between two dates: $interval->days days<br>";
        echo "Difference in years between two dates: $interval->y years<br>";
    }

}
?>
</body></html>
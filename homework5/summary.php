index.php

<?php

session_start();
$products = [
    [
        'id' => 1,
        'name' => 'AK47',
        'description' => 'rifle for T side only',
        'price' => '2700'
    ],
    [
        'id' => 2,
        'name' => 'M4A4',
        'description' => 'rifle for CT',
        'price' => '3100'
    ],
    [
        'id' => 3,
        'name' => 'DE',
        'description' => 'hand cannon',
        'price' => '700'
    ],
    [
        'id' => 4,
        'name' => 'AWP',
        'description' => '1 shot 1 kill',
        'price' => '4750'
    ]
];
$_SESSION['products'] = $products;
if ($_POST['username']) $_SESSION['username'] = $_POST['username'];
if ($_POST['password']) $_SESSION['password'] = $_POST['password'];
?>
<html>
<h1><strong>Welcome to BKSHOP</strong></h1><br>
<?php if (!$_SESSION['username'] || !$_SESSION['password']) { ?>
    <form action="login.php" method="post">
        <input type="submit" name="login" value="Login"/>
    </form>
<?php } else { ?>
    Welcome <?php echo $_SESSION['username']; ?><br>
    <form action="login.php" method="post">
        <input type="submit" name="logout" value="Logout"/>
    </form>
<?php } ?>

<table border="1">
    <thead>
    <th>Name</th>
    <th>Description</th>
    <th>Price</th>
    <th></th>
    </thead>
    <tbody>
    <?php foreach ($products as $product) { ?>
        <tr>
            <td><?php echo $product['name']; ?></td>
            <td><?php echo $product['description']; ?></td>
            <td><?php echo $product['price']; ?></td>
            <td><form action="added.php" method="post">
                    <button type="submit" name="addtocart" value="<?php echo $product['id'];?>">Add to cart</button>
                </form></td>
        </tr>
    <?php } ?>
    </tbody>
</table>

<br><a href="cart.php">Show cart</a>
</html>

added.php

<?php
/**
 * Created by PhpStorm.
 * User: joel
 * Date: 06/03/2017
 * Time: 00:59
 */
session_start();
$productId = $_POST['addtocart'];
$products = $_SESSION['products'];
$productAdded = false;

foreach ($products as $product) {
    if ($product['id'] == $productId) {
        echo $product['name'] . ' has been added';
        if (!$_SESSION['added_products']) {
            $product['qty'] = 1;
            $_SESSION['added_products'][$productId] = $product;
        } else {
            if (!isset($_SESSION['added_products'][$productId])) {
                $product['qty'] = 1;
                $_SESSION['added_products'][$productId] = $product;
            } else {
                $_SESSION['added_products'][$productId]['qty']++;
            }
        }
        $productAdded = true;
    }
}

if (!$productAdded) {
    echo 'Error! No product added';
}
?>

<br>
<a href="cart.php">Show cart</a><br>
<a href="index.php">Continue shopping</a>

cart.php

<?php
/**
 * Created by PhpStorm.
 * User: joel
 * Date: 06/03/2017
 * Time: 01:39
 */
session_start();
$cartProducts = $_SESSION['added_products'];
?>

<h3>Shopping Cart</h3>
<?php if (!count($cartProducts)) { ?>
    <div>Cart is empty</div>
<?php } else { ?>
    <table border="1">
        <thead>
        <th>Name</th>
        <th>Description</th>
        <th>Price</th>
        <th>Qty</th>
        <th></th>
        </thead>
        <tbody>
        <?php foreach ($cartProducts as $cartProduct) { ?>
            <tr>
                <td><?php echo $cartProduct['name']?></td>
                <td><?php echo $cartProduct['description']?></td>
                <td><?php echo $cartProduct['price']?></td>
                <td><?php echo $cartProduct['qty']?></td>
                <td>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>

    <a href="checkout.php">Go to checkout</a>
<?php } ?>
<a href="index.php">Back to main</a>

login.php

<?php
/**
 * Created by PhpStorm.
 * User: joel
 * Date: 06/03/2017
 * Time: 00:01
 */
session_start();
if ($_POST['logout']) {
    session_destroy();
    header('Location: index.php');
}
?>
<form action="index.php" method="post">
    Username: <input name="username" type="text" required/><br>
    Password: <input name="password" type="password" required/><br>
    <input type="submit" value="Login"/>
</form>

checkout.php

<?php
/**
 * Created by PhpStorm.
 * User: joel
 * Date: 06/03/2017
 * Time: 02:11
 */
session_start();
$cartProducts = $_SESSION['added_products'];
?>
<h3>Thank you for your order</h3>
<h4>Order number #1234</h4>
<ul>
    <?php foreach ($cartProducts as $cartProduct) {?>
        <li><?php echo $cartProduct['name'] . '; ' . $cartProduct['qty'] . ' unit(s);' ?></li>
    <?php } ?>
</ul>
<?php unset($_SESSION['added_products']); ?>
<a href="index.php">Back to main</a>

<?php

session_start();
$productId = $_POST['addtocart'];
$products = $_SESSION['products'];
$productAdded = false;

foreach ($products as $product) {
    if ($product['id'] == $productId) {
        echo $product['name'] . ' has been added';
        //if (!$_SESSION['added_products']) {
        if(!isset($_SESSION['added_products'])){
            $product['qty'] = 1;
            $_SESSION['added_products'][$productId] = $product;
        } else {
            if (!isset($_SESSION['added_products'][$productId])) {
                $product['qty'] = 1;
                $_SESSION['added_products'][$productId] = $product;
            } else {
                $_SESSION['added_products'][$productId]['qty']++;
            }
        }
        $productAdded = true;
    }
}

if (!$productAdded) {
    echo 'Error! No product added';
}
?>

<br>
<a href="cart.php">Show cart</a><br>
<a href="index.php">Continue shopping</a>

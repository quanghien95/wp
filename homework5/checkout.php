<?php

session_start();
$cartProducts = $_SESSION['added_products'];
?>
<h3>Thank you for your order</h3>
<h4>Order number #1234</h4>
<ul>
    <?php foreach ($cartProducts as $cartProduct) {?>
        <li><?php echo $cartProduct['name'] . '; ' . $cartProduct['qty'] . ' unit(s);' ?></li>
    <?php } ?>
</ul>
<?php unset($_SESSION['added_products']); ?>
<a href="index.php">Back to main</a>

<form action="homework2.php" method="post">
    Name: <input type="text" size="10" maxlenght="25" name="name">
    Class: <input type="text" size="10" maxlenght="25" name="class">
    University: <input type="text" size="10" maxlenght="25" name="university">
    <br><br>
    Date of birth:
    <select name="DOBMonth">
        <option> - Month -</option>
        <option value="January">January</option>
        <option value="Febuary">Febuary</option>
        <option value="March">March</option>
        <option value="April">April</option>
        <option value="May">May</option>
        <option value="June">June</option>
        <option value="July">July</option>
        <option value="August">August</option>
        <option value="September">September</option>
        <option value="October">October</option>
        <option value="November">November</option>
        <option value="December">December</option>
    </select>

    <select name="DOBDay">
        <option> - Day -</option>
        <?php for ($i = 1; $i <= 31; $i++) { ?>
            <option value="<?php echo $i ?>"><?php echo $i ?></option>
        <?php } ?>
    </select>

    <select name="DOBYear">
        <option> - Year -</option>
        <?php for ($i = 1950; $i <= 2010; $i++) { ?>
            <option value="<?php echo $i ?>"><?php echo $i ?></option>
        <?php } ?>
    </select>
    <br><br>
    Hobbies:
    <br>
    <input type="checkbox" name="music" value="yes"> Listening to music<br>
    <input type="checkbox" name="game" value="yes"> Playing video games<br>
    <input type="checkbox" name="sport" value="yes"> Playing sports<br>
    <input type="checkbox" name="movie" value="yes"> Watching movies<br>
    <br>
    <input type="submit" value="Submit">
</form>
<?php
if (!empty($_POST)) {
    $name = $_POST['name'];
    $class = $_POST['class'];
    $university = $_POST['university'];
    $DOBMonth = $_POST['DOBMonth'];
    $DOBDay = $_POST['DOBDay'];
    $DOBYear = $_POST['DOBYear'];
    $music = !empty($_POST['music']);
    $game = !empty($_POST['game']);
    $sport = !empty($_POST['sport']);
    $movie = !empty($_POST['movie']);
    echo "Hello, $name<br><br>";
    if (!empty($class) || !empty($university)) {
        echo "You are studying at ";
        if (!empty($class)) {
            echo "$class, ";
        }
        if (!empty($university)) {
            echo "$university";
        }
        echo "<br><br>";
    }
    if ($DOBMonth != "- Month -" && $DOBDay != "- Day -" && $DOBYear != "- Year -") {
        echo "You was born in $DOBMonth $DOBDay, $DOBYear<br><br>";
    }
    if ($music || $game || $sport || $movie) {
        echo "Your hobbies are:<br>";
        if ($music) {
            echo "- Listening to music<br>";
        }
        if ($game) {
            echo "- Playing video game<br>";
        }
        if ($sport) {
            echo "- Playing sports<br>";
        }
        if ($movie) {
            echo "- Watching movies<br>";
        }
    }
}
?>
</body></html>